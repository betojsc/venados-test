# Prueba Android Dacode - Venados Test

# NOTA
## El web service(WS) de statistics (https://venados.dacodes.mx/api/statistics) devuelve un array vacio,
## por lo tanto no se pudo hacer con WS pero se hizo la logica el dise�o y el modelo para agregar equipos
## se adjunto una imagen en la misma vista pra demostrar los valores que me devolvia el WS

## Prerrequisitos
*Tener instalado android studio, este proyecto fue creado en la version `3.5.2`

*El SDK de compilacion fue el `api 29` de google tenerlo instalado o descargalo para poder abrir el proyecto

*Tener instalado el plugin de Kotlin y actualizarlo a la version `1.3.50`

*tener la version del `build.gradle` (** no ** el archivo` build.gradle` de su m�dulo)

``gradle
dependencies {
        classpath 'com.android.tools.build:gradle:3.5.2'
}
``

## Dependencias

Este proyecto utiliza estas bibliotecas.
### Retrofit
*Type-safe HTTP client for Android and Java by Square, Inc.*
### RxJava
*RxJava is a Java VM implementation of Reactive Extensions: a library for composing asynchronous and event-based programs by using observable sequences.*
### Coil
*An image loading library for Android backed by Kotlin Coroutines. Coil is:*
