package com.betojsc.venadostest.models

data class DataGames(
    var data: InfoGame
)

data class InfoGame(
    var games: List<ListGame>,
    var code: Int
)

data class ListGame(
    var local: Boolean,
    var opponent: String,
    var opponent_image: String,
    var datetime: String,
    var league: String,
    var image: String,
    var home_score: Int,
    var away_score: Int,
    var titleSection: String,
    val section: Boolean = false
)