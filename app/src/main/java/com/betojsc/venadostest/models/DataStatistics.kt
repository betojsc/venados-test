package com.betojsc.venadostest.models

class DataStatistics (
    var data: InfoStatistics
)

class InfoStatistics(
    var statistics: List<Liststatistics>,
    var code: Int
)

class Liststatistics(
    var position:Int,
    var image: String,
    var team: String,
    var jj:Int,
    var dg:Int,
    var pts:Int
)