package com.betojsc.venadostest.models

data class DataPlayers(
    var data: InfoPlayers
)

data class InfoPlayers(
    var team : Position
)

data class Position(
    var forwards: ArrayList<DataInfo>,
    var centers: ArrayList<DataInfo>,
    var defenses: ArrayList<DataInfo>,
    var goalkeepers: ArrayList<DataInfo>,
    var coaches: ArrayList<DataInfo>
)

data class DataInfo(
    var name: String,
    var first_surname: String,
    var second_surname: String,
    var birthday: String,
    var birth_place: String,
    var weight: Float,
    var height: Float,
    var position: String,
    var number: Int,
    var position_short: String,
    var role: String,
    var role_short: String,
    var last_team: String,
    var image: String
)