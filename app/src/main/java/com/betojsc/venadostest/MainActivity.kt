package com.betojsc.venadostest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import coil.api.load
import coil.transform.RoundedCornersTransformation
import com.betojsc.venadostest.fragments.FragmentHome
import com.betojsc.venadostest.fragments.FragmentPlayers
import com.betojsc.venadostest.fragments.FragmentStatistics
import com.betojsc.venadostest.helper.replaceFragmenty
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.navigationview_header.view.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (savedInstanceState == null){
            replaceFragmenty(
                fragment = FragmentHome(),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
            nav_view.menu.getItem(0).isChecked = true
        }

        val toggle = ActionBarDrawerToggle (this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
        nav_view.getHeaderView(0)
            .imageHead.load(R.drawable.logo_venados){
            transformations(
                RoundedCornersTransformation(38f)
            )
        }
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.nav_home -> {
                replaceFragmenty(
                    fragment = FragmentHome(),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            R.id.nav_stadisticas -> {
                replaceFragmenty(
                    fragment = FragmentStatistics(),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            R.id.nav_jugadores -> {
                replaceFragmenty(
                    fragment = FragmentPlayers(),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if(drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }else{
            super.onBackPressed()
        }
    }

}
