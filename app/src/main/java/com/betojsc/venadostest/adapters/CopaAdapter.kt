package com.betojsc.venadostest.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.betojsc.venadostest.R
import com.betojsc.venadostest.models.ListGame
import kotlinx.android.synthetic.main.item_copa.view.*
import kotlinx.android.synthetic.main.item_section.view.*
import java.text.SimpleDateFormat
import java.util.*


class CopaAdapter(
    private var listCopa:List<ListGame> = emptyList()):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object{
        private const val VIEW_SECTION = 0
        private const val VIEW_ITEM = 1
    }


    private lateinit var onClickListener: OnClickListener

    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    class OriginalViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView){
        var tvLocal: TextView = itemView.text_local
        var tvOpponent: TextView = itemView.txt_visit
        var tvHomeScore: TextView = itemView.text_gol_local
        var tvAwayScore: TextView = itemView.text_gol_visit
        var imgLocal : ImageView = itemView.img_local
        var imgOpponent : ImageView = itemView.img_visit
        var imgCalendar : ImageView = itemView.calendar
        var tvDate: TextView = itemView.fecha
    }

    class SectionViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView){
        var titleSection:TextView = itemView.titleSection
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val vh:RecyclerView.ViewHolder
        when(viewType){
            VIEW_ITEM -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_copa, parent, false)
                vh =OriginalViewHolder(v)
                return vh
            }
            VIEW_SECTION -> {
                val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_section, parent, false)
                vh =SectionViewHolder(v)
                return vh
            }
        }
        throw RuntimeException("Could not inflate layout")
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val info = listCopa[position]
        if (holder is OriginalViewHolder){
            holder.tvDate.text = formatDate(info.datetime)
            when(info.local){
                true->{
                    holder.tvLocal.text = "Venados F.C."
                    holder.imgLocal.load(R.drawable.logo_venados)
                    holder.tvHomeScore.text = info.home_score.toString()
                    holder.tvOpponent.text = info.opponent
                    holder.imgOpponent.load(info.opponent_image)
                    holder.tvAwayScore.text = info.away_score.toString()
                }
                false -> {
                    holder.tvOpponent.text = "Venados F.C."
                    holder.imgOpponent.load(R.drawable.logo_venados)
                    holder.tvAwayScore.text = info.away_score.toString()
                    holder.tvLocal.text = info.opponent
                    holder.imgLocal.load(info.opponent_image)
                    holder.tvHomeScore.text = info.home_score.toString()
                }
            }
            holder.imgCalendar.setOnClickListener { v ->
                onClickListener.onItemClick(v, info, position) }
        }else{
            val view = holder as SectionViewHolder
            view.titleSection.text = formatDateSection(info.titleSection).toUpperCase()
        }

    }

    override fun getItemCount(): Int {
        return listCopa.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(listCopa[position].section){
            VIEW_SECTION
        }else{
            VIEW_ITEM
        }
    }

    interface OnClickListener {
        fun onItemClick(view: View, obj: ListGame, pos: Int)
    }


    private fun formatDate(date:String):String{
        val parser =  SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val formatter = SimpleDateFormat("dd EE", Locale.getDefault())
        return formatter.format(parser.parse(date)!!)
    }

    private fun formatDateSection(date:String):String{
        val parser =  SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val formatter = SimpleDateFormat("MMMM", Locale.getDefault())
        return formatter.format(parser.parse(date)!!)
    }
}