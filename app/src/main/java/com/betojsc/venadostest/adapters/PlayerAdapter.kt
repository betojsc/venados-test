package com.betojsc.venadostest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.CircleCropTransformation
import com.betojsc.venadostest.R
import com.betojsc.venadostest.models.DataInfo
import kotlinx.android.synthetic.main.item_players.view.*

class PlayerAdapter (
    private var listPlayer:ArrayList<DataInfo>):
    RecyclerView.Adapter<PlayerAdapter.MyViewHolder>() {
    private lateinit var onClickListener: OnClickListener

    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgPlayer : ImageView = itemView.imgPlayer
        var txtPosition: TextView = itemView.txtPosition
        var txtName: TextView = itemView.txtName
        var lytParent: LinearLayout = itemView.lyt_parent
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_players, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val info = listPlayer[position]
        val fullName = String.format("%s %s %s",info.name,info.first_surname,info.second_surname)
        holder.txtName.text = fullName
        if (info.role != null){
            holder.txtPosition.text = info.role
        }else{
            holder.txtPosition.text = info.position
        }
        holder.imgPlayer.load(info.image){
            transformations(CircleCropTransformation())
        }

        holder.lytParent.setOnClickListener { v ->
            onClickListener.onItemClick(v, info, position)
        }
    }

    override fun getItemCount(): Int {
        return listPlayer.size
    }

    interface OnClickListener {
        fun onItemClick(view: View, obj: DataInfo, pos: Int)
    }
}