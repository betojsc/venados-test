package com.betojsc.venadostest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.betojsc.venadostest.R
import com.betojsc.venadostest.models.Liststatistics
import kotlinx.android.synthetic.main.item_statistics.view.*

class StatisticsAdapter (private var listStatistics:ArrayList<Liststatistics>):
    RecyclerView.Adapter<StatisticsAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var Position: TextView = itemView.tvPosition
        var imgTeam: ImageView = itemView.imgImageTeam
        var Team: TextView = itemView.tvTeam
        var jj: TextView = itemView.tvJj
        var dg: TextView = itemView.tvDg
        var pts: TextView = itemView.tvPts
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_statistics, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val info = listStatistics[position]
        holder.Position.text = info.position.toString()
        holder.imgTeam.load(R.drawable.logo_venados)
        holder.Team.text = info.team
        holder.jj.text = info.jj.toString()
        holder.dg.text = info.dg.toString()
        holder.pts.text = info.pts.toString()
    }

    override fun getItemCount(): Int {
        return listStatistics.size
    }
}