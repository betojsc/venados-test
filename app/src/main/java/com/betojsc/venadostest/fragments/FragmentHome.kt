package com.betojsc.venadostest.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.betojsc.venadostest.R
import com.betojsc.venadostest.helper.HelperPagerAdapter

import kotlinx.android.synthetic.main.fragment_home.*

class FragmentHome : Fragment(){

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = HelperPagerAdapter(childFragmentManager)
        adapter.addFragment(FragmentCopa(), "COPA MX")
        adapter.addFragment(FragmentLiga(), "LIGA MX")
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }
}