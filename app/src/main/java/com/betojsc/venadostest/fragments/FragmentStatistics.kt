package com.betojsc.venadostest.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.betojsc.venadostest.R
import com.betojsc.venadostest.adapters.StatisticsAdapter
import com.betojsc.venadostest.api.APIService
import com.betojsc.venadostest.helper.Client
import com.betojsc.venadostest.models.DataStatistics
import com.betojsc.venadostest.models.Liststatistics
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.IoScheduler
import kotlinx.android.synthetic.main.fragment_statistics.*

class FragmentStatistics : Fragment(){
    private lateinit var adapter: StatisticsAdapter
    private val apiService = Client.getClient().create(APIService::class.java)
    val list: ArrayList<Liststatistics> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statistics, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getStatistics()

        rv_statistics.layoutManager = LinearLayoutManager(requireContext())
        adapter = StatisticsAdapter(list)
        adapter.notifyDataSetChanged()
    }

    private fun getStatistics(){
        val call = apiService.getStatistics()
        call.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(IoScheduler())
            .subscribe(object: Observer<DataStatistics>{
                override fun onNext(t: DataStatistics) {
                    Log.e("onNext",t.data.statistics.toString())
                    rv_statistics.refreshDrawableState()
                    list.add(Liststatistics(position = 1,team = "Venados F.C.",image = "image",
                        jj = 2,dg = 5,pts = 6))
                    rv_statistics.adapter = adapter
                    rv_statistics.itemAnimator.apply {
                        if (this is DefaultItemAnimator) {
                            supportsChangeAnimations = false
                        }
                    }
                    adapter.notifyDataSetChanged()
                    progress_bar.visibility = View.GONE
                    imgError.visibility = View.VISIBLE
                    tvError.visibility = View.VISIBLE
                }
                override fun onComplete() {
                    Log.e("onComplete","true")
                }
                override fun onSubscribe(d: Disposable) {
                    Log.e("onSubscribe","true")
                }

                override fun onError(e: Throwable) {
                    Log.e("onError",e.message.toString())
                }

            })
    }
    private fun data(){

    }
}