package com.betojsc.venadostest.fragments

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.GONE
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import coil.api.load
import coil.transform.CircleCropTransformation
import com.betojsc.venadostest.R
import com.betojsc.venadostest.adapters.PlayerAdapter
import com.betojsc.venadostest.api.APIService
import com.betojsc.venadostest.helper.Client
import com.betojsc.venadostest.models.DataInfo
import com.betojsc.venadostest.models.DataPlayers
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.IoScheduler
import kotlinx.android.synthetic.main.layout_dialog.*
import kotlinx.android.synthetic.main.fragment_players.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FragmentPlayers : Fragment(){
    private lateinit var adapter: PlayerAdapter
    private val apiService = Client.getClient().create(APIService::class.java)
    private val allList: ArrayList<DataInfo> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_players, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getPlayers()
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.title = "Jugadores"
        rv_players.layoutManager = GridLayoutManager(requireContext(), 3)
        rv_players.setHasFixedSize(true)

        adapter = PlayerAdapter(allList)
        adapter.setOnClickListener(object : PlayerAdapter.OnClickListener{
            override fun onItemClick(view: View, obj: DataInfo, pos: Int) {
                val fullName = String.format(" %s %s %s",obj.name,obj.first_surname,
                    obj.second_surname)

                val birthday = obj.birthday.split("T").toTypedArray()
                dialogInfoPlayer(fullName,obj.position,obj.image,formatDate(birthday[0]),
                    obj.birth_place, obj.weight.toString(),obj.height.toString(),obj.last_team,
                    obj.role)
            }

        })
        adapter.notifyDataSetChanged()
    }

    private fun dialogInfoPlayer(name:String,position:String,image:String,birthday:String,
                                 lugar:String,peso:String,altura:String,eqpAnterior:String,
                                 role:String){
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.layout_dialog)
        dialog.setCancelable(true)
        Objects.requireNonNull<Window>(dialog.window)
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(Objects.requireNonNull<Window>(dialog.window).attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT

        dialog.dialogImage.load(image){
            transformations(CircleCropTransformation())
        }
        dialog.dialogName.text = name
        if (role == null){
            dialog.dialogPosition.text = position
        }else{
            dialog.dialogPosition.text = role
        }


        dialog.dialogDate.text = birthday
        dialog.dialogLugarN.text = lugar
        dialog.dialogPeso.text = "$peso KG"
        dialog.dialogAltura.text = "$altura M"
        dialog.dialogEquAnterior.text = eqpAnterior

        dialog.show()
        dialog.window!!.attributes = lp
    }

    private fun formatDate(date:String):String{
        val parser =  SimpleDateFormat("yyyy-MM-dd",Locale.getDefault())
        val formatter = SimpleDateFormat("dd/MM/yyyy",Locale.getDefault())
        return formatter.format(parser.parse(date)!!)
    }

    private fun getPlayers(){
        val call = apiService.getPlayers()
        call.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(IoScheduler())
            .subscribe(object: Observer<DataPlayers> {
                override fun onNext(t: DataPlayers) {
                    allList.addAll(t.data.team.forwards)
                    allList.addAll(t.data.team.centers)
                    allList.addAll(t.data.team.defenses)
                    allList.addAll(t.data.team.goalkeepers)
                    allList.addAll(t.data.team.coaches)
                    rv_players.adapter = adapter
                    rv_players.itemAnimator.apply {
                        if (this is DefaultItemAnimator) {
                            supportsChangeAnimations = false
                        }
                    }
                    adapter.notifyDataSetChanged()
                    rv_players.refreshDrawableState()
                    progress_bar.visibility = GONE
                }
                override fun onComplete() {
                    Log.e("onComplete","true")
                }
                override fun onSubscribe(d: Disposable) {
                    Log.e("onSubscribe","true")
                }
                override fun onError(e: Throwable) {
                    Log.e("onError",e.message.toString())
                }
            })
    }

}