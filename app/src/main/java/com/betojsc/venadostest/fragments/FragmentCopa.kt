package com.betojsc.venadostest.fragments

import android.content.Intent
import android.os.Bundle
import android.provider.CalendarContract
import android.provider.CalendarContract.Events.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.betojsc.venadostest.R
import com.betojsc.venadostest.adapters.CopaAdapter
import com.betojsc.venadostest.api.APIService
import com.betojsc.venadostest.helper.Client
import com.betojsc.venadostest.models.DataGames
import com.betojsc.venadostest.models.ListGame
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.IoScheduler
import kotlinx.android.synthetic.main.fragment_copa.*
import kotlinx.android.synthetic.main.fragment_copa.progress_bar
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FragmentCopa : Fragment(){
    private lateinit var adapter: CopaAdapter
    private val apiService = Client.getClient().create(APIService::class.java)
    val listCopaMX: ArrayList<ListGame> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_copa, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getGames()

        rv_copa.layoutManager = LinearLayoutManager(requireContext())

        adapter = CopaAdapter(listCopaMX)
        adapter.setOnClickListener(object : CopaAdapter.OnClickListener{
            override fun onItemClick(view: View, obj: ListGame, pos: Int) {
                val date = formatDate(obj.datetime)
                val title = String.format("Venados F.C. VS %s",obj.opponent)
                dialogAlert(title,date)
            }

        })
        adapter.notifyDataSetChanged()
    }

    private fun getGames(){
        val call = apiService.getGames()
        call.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(IoScheduler())
            .subscribe(object: Observer<DataGames> {
                override fun onNext(t: DataGames) {
                    for (item in t.data.games) {
                        if (item.league == "Copa MX"){
                            listCopaMX.add(ListGame(item.local,item.opponent,
                                item.opponent_image,item.datetime,item.league,item.image,
                                item.home_score,item.away_score, item.datetime,true))
                            listCopaMX.add(ListGame(item.local,item.opponent,
                                item.opponent_image,item.datetime,item.league,item.image,
                                item.home_score,item.away_score,"",false))
                        }
                    }
                    rv_copa.adapter = adapter
                    rv_copa.itemAnimator.apply {
                        if (this is DefaultItemAnimator) {
                            supportsChangeAnimations = false
                        }
                    }
                    adapter.notifyDataSetChanged()
                    rv_copa.refreshDrawableState()
                    progress_bar.visibility = View.GONE
                }
                override fun onComplete() {
                    Log.e("onComplete","true")
                }
                override fun onSubscribe(d: Disposable) {
                    Log.e("onSubscribe","true")
                }
                override fun onError(e: Throwable) {
                    Log.e("onError",e.message.toString())
                }
            })
    }

    private fun dialogAlert(title: String,date: String){
        AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(R.string.messageAlert)
            .setPositiveButton(R.string.ok) { _, _ ->
                addCalendarEvent(title,date)
            }
            .setNegativeButton(R.string.cancel, null)
            .setIcon(android.R.drawable.ic_menu_info_details)
            .show()
    }
    private fun addCalendarEvent(title:String,date: String){
        val intent = Intent(Intent.ACTION_INSERT)
            .setData(CONTENT_URI)
            .putExtra(TITLE,title)
            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,convertMilis("$date, 19:00"))
            .putExtra(CalendarContract.EXTRA_EVENT_END_TIME,convertMilis("$date, 21:00"))
        startActivity(intent)
    }

    private fun formatDate(date:String):String{
        val parser =  SimpleDateFormat("yyyy-MM-dd",Locale.getDefault())
        val formatter = SimpleDateFormat("dd/MM/yyyy",Locale.getDefault())
        return formatter.format(parser.parse(date)!!)
    }

    private fun convertMilis(date: String): Long{
        val formatter = SimpleDateFormat("dd/MM/yyyy, HH:mm",Locale.getDefault())
        val oldDate = formatter.parse(date)
        return oldDate?.time!!
    }
}