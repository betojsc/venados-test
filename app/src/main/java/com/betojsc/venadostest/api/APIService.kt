package com.betojsc.venadostest.api


import com.betojsc.venadostest.models.DataGames
import com.betojsc.venadostest.models.DataPlayers
import com.betojsc.venadostest.models.DataStatistics
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*

interface APIService {
    @Headers(
        "Accept: application/json"
    )
    //Games
    @GET("api/games")
    fun getGames(): Observable<DataGames>

    @Headers(
        "Accept: application/json"
    )
    //players
    @GET("api/players")
    fun getPlayers(): Observable<DataPlayers>

    @Headers(
        "Accept: application/json"
    )
    //statistics
    @GET("api/statistics")
    fun getStatistics(): Observable<DataStatistics>

}